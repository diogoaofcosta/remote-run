#!/bin/bash

# Copyright 2019 Diogo Costa
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

set -e

CONFIG_FILENAME="./.rrun.config"
REMOTE_HOST=
REMOTE_PORT=22
REMOTE_USER="$USER"
REMOTE_IDENTITY=
LOCAL_INCLUDE=
LOCAL_EXCLUDE=
REMOTE_INCLUDE=
REMOTE_EXCLUDE=

EXIT_CODE_NO_REMOTE_HOST=1
EXIT_CODE_NO_REMOTE_PORT=2
EXIT_CODE_NO_REMOTE_USER=3
EXIT_CODE_NO_REMOTE_IDENTITY=4
EXIT_CODE_NO_COMMAND=5

die() {
    local exit_code="$1"; shift
    local msg="$*"

    >&2 echo "$msg"
    exit "$exit_code"
}

validate_config() {
    [ -n "$REMOTE_HOST" ] || die $EXIT_CODE_NO_REMOTE_HOST "Missing remote.host config"
    [ -n "$REMOTE_PORT" ] || die $EXIT_CODE_NO_REMOTE_PORT "Missing remote.port config"
    [ -n "$REMOTE_USER" ] || die $EXIT_CODE_NO_REMOTE_USER "Missing remote.user config"
    [ -z "$REMOTE_IDENTITY" ] || [ -f "$REMOTE_IDENTITY" ] \
        || die $EXIT_CODE_NO_REMOTE_IDENTITY "Invalid file in remote.identity"
}

load_config() {
    if [ ! -f "$CONFIG_FILENAME" ]; then
        die 1 "Could not find configuration file."
    fi

    IFS="="
    while read -r name value; do
        case $name in
            remote.host) REMOTE_HOST="$value";;
            remote.port) REMOTE_PORT="$value";;
            remote.user) REMOTE_USER="$value";;
            remote.path) REMOTE_PATH="$value";;
            remote.identity) REMOTE_IDENTITY="$value";;
            remote.include) REMOTE_INCLUDE="$value";;
            remote.exclude) REMOTE_EXCLUDE="$value";;
            local.include) LOCAL_INCLUDE="$value";;
            local.exclude) LOCAL_EXCLUDE="$value";;
            \#*) ;; # Ignore commented line
            "") ;; # Ignore empty line
            *) echo "WARN: Unknown key [$name] ignored.";;
        esac
    done < "$CONFIG_FILENAME"
    unset IFS

    validate_config
}

get_ssh_flags() {
    declare -a ssh_flags

    ssh_flags+=(-q) # quiet
    ssh_flags+=(-p "$REMOTE_PORT") # remote port
    ssh_flags+=(-l "$REMOTE_USER") # remote user
    ssh_flags+=(-o "StrictHostKeyChecking=no") # don't check remote cert
    ssh_flags+=(-o "UserKnownHostsFile=/dev/null") # don't update known hosts file
    ssh_flags+=(-T) # disable pseudo-terminal
    ssh_flags+=(-x) # disable X11 Forwarding
    
    if [ -n "$REMOTE_IDENTITY" ]; then
        ssh_flags+=(-i "$REMOTE_IDENTITY") # set custom identity file
    fi

    echo "${ssh_flags[@]}"
}

execute_cmd() {
    local cmd="$*"

    [ -n "$cmd" ] || die $EXIT_CODE_NO_COMMAND "No command provided."

    # Disable shellcheck quoting rules because we want the vars
    # to be splitted.
    #
    # shellcheck disable=SC2046,SC2086
    ssh $(get_ssh_flags) "$REMOTE_HOST" cd "$REMOTE_PATH"\; $cmd
}

push_local_workspace() {
    declare -a rsync_flags=(-azr)

    if [ -n "$LOCAL_INCLUDE" ]; then
        IFS=,
        for p in $LOCAL_INCLUDE; do
            rsync_flags+=(--include'='"$p")
        done
        unset IFS
    fi

    if [ -n "$LOCAL_EXCLUDE" ]; then
        IFS=,
        for p in $LOCAL_EXCLUDE; do
            rsync_flags+=(--exclude'='"$p")
        done
        unset IFS
    fi

    echo "Pushing local workspace to $REMOTE_HOST:$REMOTE_PATH..."

    RSYNC_RSH="ssh $(get_ssh_flags)"
    export RSYNC_RSH

    rsync "${rsync_flags[@]}" . "$REMOTE_HOST":"$REMOTE_PATH"
}

pull_remote_workspace() {
    declare -a rsync_flags=(-azr)

    if [ -n "$REMOTE_INCLUDE" ]; then
        IFS=,
        for p in $REMOTE_INCLUDE; do
            rsync_flags+=(--include'='"$p")
        done
        unset IFS
    fi

    if [ -n "$REMOTE_EXCLUDE" ]; then
        IFS=,
        for p in $REMOTE_EXCLUDE; do
            rsync_flags+=(--exclude'='"$p")
        done
        unset IFS
    fi

    echo "Pulling remote workspace from $REMOTE_HOST:$REMOTE_PATH..."

    RSYNC_RSH="ssh $(get_ssh_flags)"
    export RSYNC_RSH

    rsync "${rsync_flags[@]}" "$REMOTE_HOST":"$REMOTE_PATH" .
}

load_config
push_local_workspace
execute_cmd "$@"
pull_remote_workspace



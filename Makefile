SHELL := /bin/bash
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
.ONESHELL:
.RECIPEPREFIX = >

all: lint test
.PHONY: all

test: build/test.log
.PHONY: test

build/test.log: rrun.sh test/demo.sh test/Vagrantfile
> mkdir -p build
> (
>   cd test
>   rm -rf out
>   vagrant up 
>   ../rrun.sh ./demo.sh 
>   -vagrant destroy -f
>   [ -d out ] && [ -f out/hello.txt ] || (echo "out/hello.txt does not exist" && exit 1)
> ) | tee build/test.log

clean:
> rm -rf build/
> rm -rf test/out/ test/*.log
.PHONY: clean

lint:
> shellcheck -f gcc rrun.sh
.PHONY: lint


# Remote Run

Syncs the current folder to a remote server, executes a command and syncs back the results to the
local box.

Use it to delegate heavy build actions to a beefier server in the cloud.

## Setup

You need access to a remote server. Make sure you are able to SSH into the server using Public Key
authentication.

There are several tutorials online on how to set up public key authentication.
[Here's one of such pages](https://www.cyberciti.biz/faq/how-to-set-up-ssh-keys-on-linux-unix/),
courtesy of [nixCraft](https://www.cyberciti.biz).

Finally, set the server details in the configuration file: `.rrun.config`.
See the [Configuration file](#Configuration%20file) section for instructions.

## Usage

Prefix any command with `rrun.sh`, i.e: `rrun.sh ./mvnw package`.

The folder you run the command from will be `rsync`-ed to the remote server and the provided command
will be executed over SSH. When it finishes, the remote folder is `rsync`-ed back to the local
machine.

## Configuration file

The script needs to know which files to sync and which remote server will do the heavy lifting.
The configuration details must be provided in a file named `.rrun.config` in the root of the
workspace.

The configuration file is a simple plain text file where:

1. Each line represents a key-value pair
2. Keys are separated from their values by a `=` sign
3. Blank lines and lines that start with `#` are ignored

The following keys are supported:

- *remote.host*: Hostname/IP of the remote server. Required.
- *remote.port*: Port where the SSH is exposed. Default: 22
- *remote.user*: Username to use when opening a new SSH connection. Required.
- *remote.path*: Path in the remote server where files are to be uploaded to. Required.
- *remote.identity*: Path in the local machine to the private key. Default: system default.
- *remote.include*: Comma separated list of files and directories to sync back from the remote
  server. Default: sync everything.
- *local.exclude*: Comma separated list of files and directories to sync to the remote server.
  Default: sync everything.

Example:

    # this is a box created using vagrant and configured with a static ip
    remote.host=172.16.0.2
    remote.port=22
    remote.user=vagrant
    remote.path=/home/vagrant/tmp/
    # vagrant places the private key in the following hidden folder
    remote.identity=.vagrant/machines/default/virtualbox/private_key
    remote.include=out/
    local.exclude=out/,.vagrant/

Check `test/.rrun.config` for an example.

## Requirements

### To run the script

Both the local and remote machines need to have `rsync` installed.

### To develop

Running the `make lint` and `make test` commands require:

- vagrant
- shellcheck


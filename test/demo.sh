#!/bin/bash

echo "Running on $(hostname)!"

if [ ! -d out ]; then
    echo "Creating out dir..."
    mkdir out
fi

echo "Hello world from $(hostname)" > out/hello.txt

echo "Done."

